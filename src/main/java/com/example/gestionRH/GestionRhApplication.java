package com.example.gestionRH;

import com.example.gestionRH.model.Personne;
import com.example.gestionRH.repoitory.PersonneRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GestionRhApplication {

	public static void main(String[] args) {

		SpringApplication.run(GestionRhApplication.class, args);
	}

	@Bean
	CommandLineRunner initDataBaseH2(PersonneRepository personneRepository){
		return args -> {
			personneRepository.save(new Personne(Long.parseLong("1"),"Ahmed","Zagouat", Long.parseLong("30")));
			personneRepository.save(new Personne(Long.parseLong("2"),"Chayma","Juoini", Long.parseLong("28")));
			personneRepository.save(new Personne(Long.parseLong("3"),"AhmedChayma","Zagouat", Long.parseLong("58")));
		};
	}
}
