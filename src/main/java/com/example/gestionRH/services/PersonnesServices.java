package com.example.gestionRH.services;

import com.example.gestionRH.repoitory.PersonneRepository;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PersonnesServices {

    @Autowired
    private PersonneRepository repo;

    public List<Long> getAge() {
        return repo.findAll().stream().map(p -> p.getAge()).toList();
    }

    public int calculer(@NotNull  int a, @NotNull int b,@NotNull String ope)  {
        int res = 0;

        switch (ope) {
            case "+":
               return res = a + b;
            case "*":
                return res = a * b;
            case "/":
                return res = a / b;
            case "-":
                return res = a - b;
        }
         return res;
    }

    public boolean verif (int a, int b){
        if(a == b)
            return true;

        return false;
    }

    public void methode(){
        List<Integer> numeros = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4)); // [1, 2, 3, 4]
        List<Integer> cc = new ArrayList<>();
        cc =  numeros.stream().map(x -> x+x).toList();

        List<List<Integer>> llis =  new ArrayList<List<Integer>>(Arrays.asList(
                new ArrayList<Integer>(Arrays.asList(1, 2)),
                new ArrayList<Integer>(Arrays.asList(3, 4))
        ));

        cc = llis.stream().flatMap(s -> s.stream()).collect(Collectors.toList());


    }
}
