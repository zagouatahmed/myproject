package com.example.gestionRH.controller;

import com.example.gestionRH.model.Personne;
import com.example.gestionRH.repoitory.PersonneRepository;
import com.example.gestionRH.services.PersonnesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@Controller
public class PersonneController {

    @Autowired
    private PersonneRepository repo;

    @Autowired
    private PersonnesServices services;


    @GetMapping(path = "/personne")
    public String personne(Model model){
       List<Personne> personnePage = repo.findAll();
       model.addAttribute("personnes",personnePage);
       return "personne";
    }

    @GetMapping("/personnes/{id}")
    public Personne findById(@PathVariable  Long id) throws Exception{
        return repo.findById(id).orElseThrow(() -> new Exception("Aucun Personne trouvé"));

    }

    @GetMapping("/personnes/age")
    public List<Long> getAge(){
       return services.getAge();
    }

    @GetMapping("/calc/{a}/{b}/{ope}")
    public int getRes (@PathVariable  int a,@PathVariable  int b,@PathVariable String ope){
        return services.calculer(a,b,ope);
    }
}
