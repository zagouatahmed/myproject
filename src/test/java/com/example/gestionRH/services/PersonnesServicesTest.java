package com.example.gestionRH.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class PersonnesServicesTest {
    PersonnesServices srv = new PersonnesServices();

    @Test
    void calculer() {

        Assertions.assertEquals(5, srv.calculer(3,2, "+"),"Addition");
        Assertions.assertEquals(6, srv.calculer(3,2, "*"),"Multiplication");
        Assertions.assertEquals(3, srv.calculer(6,2, "/"), "Division");
        Assertions.assertEquals(1, srv.calculer(3,2, "-"), "Sabstraction");

    }

    @Test
    void verif(){

        assertTrue(srv.verif(2,2),"true zebi");
        assertFalse(srv.verif(2,3),"false zebi");
    }
}